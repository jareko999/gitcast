## Gitcast

Landing page for Gitlab webcast

## Hosted at...

[https://dailyclick.io/gitcast](https://dailyclick.io/gitcast)

## Run it locally

For Mac:

Run `python -m SimpleHTTPServer`

Then head to `http://localhost:8000`
