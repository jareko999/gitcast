var rightContainer = document.getElementById('right-container');
var covers = 40;

// Arrays to store durations and delays for animations //
var randomDelays = []
var randomDurations = []


function load() {
	// Animate info in //
	TweenMax.staggerFrom(".greensock", .6, {y:"20", opacity:0, ease: Power4.easeOut}, 0.08);

	// Create duration and delay #s and add them to arrays //
	delayCount = 0
	durationCount = 4
	for (var i = 0; i < 10; i++) {
		delayCount += .1
		durationCount += .4
		randomDelays.push(delayCount.toFixed(1))
		randomDurations.push(durationCount.toFixed(1))
	}
	// Once the arrays are filled, create divs //
	createCovers()
}

function createCovers() {
	var top = 0
	var coverHeight = window.innerHeight/covers
	for (var i = 0; i < covers; i++) {
		var randomIndex = Math.floor(Math.random() * randomDelays.length)
		var randomDurationIndex = Math.floor(Math.random() * randomDurations.length) + 4
		var cover = document.createElement('DIV');
		cover.classList.add('code-cover');
		cover.style.height = coverHeight + "px"
		cover.style.top = top + "px"
		cover.style.animation = "reveal " + randomDurationIndex + "s cubic-bezier(1,.4,.4,1) infinite";
		cover.style.animationDelay = randomIndex + "s";
		rightContainer.appendChild(cover)
		top += coverHeight
	}
}

load();
